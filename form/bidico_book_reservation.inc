<?php

/**
 *
 * Form per la prenotazione di un libro
 */
function bidico_book_reservation_form($form, &$form_state, $book_node_id)
{
    //Recupera il nodo per il quale si vuole effettuare la prenotazione
    $book_node = node_load($book_node_id);

    if (!isset($book_node->title) || $book_node->type != 'book') {
        $form['message'] = array('#markup' => t("I can't find your book."));
    } else {
        $message = t("Do you want to reserve this book?");
        $message .= "<p>" . $book_node->title . "</p>";

        //Stampa il titolo del libro da prenotare
        $form['message'] = array('#markup' => $message);

        //Imposta l'id del libro da prenotare com valore di un campo hidden
        $form['book_node_id'] = array(
            '#type' => 'hidden',
            '#value' => $book_node_id,
            '#access' => FALSE
        );

        //Pulsante di invio del form
        $form['submit_button'] = array(
            '#type' => 'submit',
            '#value' => t('Yes!'),
        );

        //Link per l'annullamento: se si annulla ritorna alla pagina di visualizzazione del libro
        $form['link'] = array('#markup' => l(t('Nope'), 'node/' . $book_node_id));
    }
    return $form;
}

/**
 *
 * Handle reservation form submission
 */
function bidico_book_reservation_form_submit($form, &$form_state)
{
    //Recupera l'id del libro che si vuole prenotare
    $book_node_id = $form_state['values']['book_node_id'];

    //Recupera il nodo per il quale si vuole effettuare la prenotazione
    $book_node = node_load($book_node_id);

    if ($book_node == null || $book_node->type != 'book'
        || $book_node->bidico_book_state['und'][0]['value'] != 'available'
    ) {
        $message = t("Your reservation failed. We are sorry: the book is no more available");
        $message_type = 'error';
    } else {
        //Change book state
        $book_node->bidico_book_state['und'][0]['value'] = 'reserved';
        //Salva il nodo modificato
        node_save($book_node);

        //Crea una nuova transition
        db_insert('transitions')->fields(array(
            'book_id' => $book_node_id,
            'state' => 'reserved',
            'owner_id' => $book_node->uid,
            'borrower_id' => $GLOBALS['user']->uid,
            'approved' => 0,
            'date' => time(),
            'notification' => 0
        ))->execute();

        $message = t("Your reservation is successful. Wait for owner approval.");
        $message_type = 'status';

        //Invia una email di notifica al owner
        $account = user_load($book_node->uid);
        $params['account'] = $account;
        drupal_mail('bidico', 'reservation', $account->mail, user_preferred_language($account), $params, 'bidico@bidico.altervista.org');
    }

    drupal_set_message($message, $message_type);

    //Redirect to homepage
    drupal_goto('<front>');
}

/**
 *
 * Form per l'approvazione di una prenotazione da parte dell'owner
 */
function bidico_reservation_approve_form($form, &$form_state, $transition_id)
{
    //Recupera la transazione che si vuole confermare
    $transition = db_select('transitions', 't')
        ->fields('t')
        ->condition('id', $transition_id, '=')
        ->execute()
        ->fetchAssoc();

    //Controlla che esista la transazione, che lo stato sia reserved e che l'owner sia l'utente loggato
    if (!isset($transition['id']) || $transition['state'] != 'reserved' || $transition['approved'] != 0
        || $transition['owner_id'] != $GLOBALS['user']->uid
    ) {
        $form['message'] = array('#markup' => t("Sorry. There was an error."));
    } else {
        $message = t("<p>Do you want to confirm the reservation?</p>");

        $form['message'] = array('#markup' => $message);

        //Imposta l'id della transazione da approvare come valore di un campo hidden
        $form['transition_id'] = array(
            '#type' => 'hidden',
            '#value' => $transition_id,
            '#access' => FALSE
        );

        //Pulsante di invio del form
        $form['submit_button'] = array(
            '#type' => 'submit',
            '#value' => t('Yes!'),
        );

        //Link per l'annullamento: se si annulla ritorna alla pagina di visualizzazione del libro
        $form['link'] = array('#markup' => l(t('Nope'), 'bidico/transitions'));
    }
    return $form;
}

/**
 *
 * Invio del form per approvare la prenotazione di un libro
 */
function bidico_reservation_approve_form_submit($form, &$form_state)
{
    //Recupera l'id della transizione passata come parametro dal form
    $transition_id = $form_state['values']['transition_id'];

    //Recupera la transazione che si vuole confermare
    $transition = db_select('transitions', 't')
        ->fields('t')
        ->condition('id', $transition_id, '=')
        ->execute()
        ->fetchAssoc();

    //Controlla che esista la transazione, che lo stato sia reserved e che l'owner sia l'utente loggato
    if (!isset($transition['id']) || $transition['state'] != 'reserved' || $transition['approved'] != 0
        || $transition['owner_id'] != $GLOBALS['user']->uid
    ) {
        $message = t("You can't approve this reservation.", "error");
    } else {
        //Recupera il nodo per il quale si vuole effettuare la prenotazione
        $book_node = node_load($transition['book_id']);

        //Change book state
        $book_node->bidico_book_state['und'][0]['value'] = 'borrowed';

        //Salva il nodo modificato
        node_save($book_node);

        //Aggiorna la transizione come approvata
        db_update('transitions')->fields(array(
                'approved' => 1,
            ))
            ->condition('id', $transition_id, '=')
            ->execute();

        //Aggiungi una nuova transizione
        db_insert('transitions')->fields(array(
            'book_id' => $book_node->nid,
            'state' => 'borrowed',
            'owner_id' => $transition['owner_id'],
            'borrower_id' => $transition['borrower_id'],
            'approved' => 1,
            'date' => time(),
            'notification' => 0
        ))->execute();

        $message = t('Reservation approved!');

        //Invia una email di notifica al borrower
        $account = user_load($transition['borrower_id']);
        $params['account'] = $account;
        drupal_mail('bidico', 'reservation_approved', $account->mail, user_preferred_language($account), $params, 'bidico@bidico.altervista.org');
    }

    drupal_set_message($message);
    drupal_goto('<front>');
}

/**
 *
 * Form per negare una prenotazione da parte dell'owner
 */
function bidico_reservation_deny_form($form, &$form_state, $transition_id)
{
    //Recupera la transazione che si vuole confermare
    $transition = db_select('transitions', 't')
        ->fields('t')
        ->condition('id', $transition_id, '=')
        ->execute()
        ->fetchAssoc();

    //Controlla che esista la transazione, che lo stato sia reserved e che l'owner sia l'utente loggato
    if (!isset($transition['id']) || $transition['state'] != 'reserved' || $transition['approved'] != 0
        || $transition['owner_id'] != $GLOBALS['user']->uid
    ) {
        $form['message'] = array('#markup' => t("Sorry. There was an error."));
    } else {
        $message = "<p>" . t("Do you want to deny the reservation and make the book available?") . "</p>";

        $form['message'] = array('#markup' => $message);

        //Imposta l'id della transazione da negare come valore di un campo hidden
        $form['transition_id'] = array(
            '#type' => 'hidden',
            '#value' => $transition_id,
            '#access' => FALSE
        );

        //Pulsante di invio del form
        $form['submit_button'] = array(
            '#type' => 'submit',
            '#value' => t('Yes!'),
        );

        //Link per l'annullamento: se si annulla ritorna alla pagina di visualizzazione del libro
        $form['link'] = array('#markup' => l(t('Nope'), 'bidico/transitions'));
    }

    return $form;
}

/**
 *
 * Invio del form per negare la prenotazione di un libro
 */
function bidico_reservation_deny_form_submit($form, &$form_state)
{
    //Recupera l'id della transizione passata come parametro dal form
    $transition_id = $form_state['values']['transition_id'];

    //Recupera la transazione che si vuole confermare
    $transition = db_select('transitions', 't')
        ->fields('t')
        ->condition('id', $transition_id, '=')
        ->execute()
        ->fetchAssoc();

    //Controlla che esista la transazione, che lo stato sia reserved e che l'owner sia l'utente loggato
    if (!isset($transition['id']) || $transition['state'] != 'reserved' || $transition['approved'] != 0
        || $transition['owner_id'] != $GLOBALS['user']->uid
    ) {
        $message = t("You can't approve this reservation.", "error");
    } else {
        //Recupera il nodo per il quale si vuole effettuare la prenotazione
        $book_node = node_load($transition['book_id']);

        //Change book state
        $book_node->bidico_book_state['und'][0]['value'] = 'available';

        //Salva il nodo modificato
        node_save($book_node);

        //Aggiorna la transizione come cancellata ( valore intero 2 )
        db_update('transitions')->fields(array(
            'approved' => 2,
        ))
            ->condition('id', $transition_id, '=')
            ->execute();

        $message = t('Reservation cancelled!');

        //Invia una email di notifica al borrower
        $account = user_load($transition['borrower_id']);
        $params['account'] = $account;
        drupal_mail('bidico', 'reservation_denied', $account->mail, user_preferred_language($account), $params, 'bidico@bidico.altervista.org');

    }

    drupal_set_message($message);
    drupal_goto('<front>');
}