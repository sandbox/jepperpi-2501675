<?php

function bidico_match_book_form($form, &$form_state)
{

    if (!empty($form_state['results_table'])) {
        $form['results_table'] = array('#markup' => $form_state['results_table']);
    }

    $form['bidico_search_term'] = array(
        '#type' => 'textfield',
        '#title' => t('Search term'),
        '#description' => "Insert a search term (e.g. ISBN or title or title and author)",
        '#required' => TRUE,
    );

    $form['submit_button'] = array(
        '#type' => 'submit',
        '#value' => t('Find!'),
    );

    return $form;
}

function bidico_match_book_form_submit($form, &$form_state)
{
    $form_state['results_table'] = get_remote_content($form_state['values']['bidico_search_term']);
    $form_state['rebuild'] = TRUE;
}

/**
 * Recupera le informazioni sul libro da inserire da google books
 *
 * https://developers.google.com/books/
 */
function get_remote_content($search_term)
{
    $api_end_point = "https://www.googleapis.com/books/v1/volumes?q=";

    $api_param = urlencode($search_term);

    //Chiamata api a google books
    $result = drupal_http_request($api_end_point . $api_param);

    //Se il codice è 200 (ok) o 304 (Not modified)
    if (in_array($result->code, array(200, 304))) {
        return print_book_matching_list($result->data);
    } else { //Se la pagina restituisce un messaggio di errore
        return "<p>Si è verificato un errore: controllare la connessione.</p>";
    }
}

/**
 * Funziona responsabile di mettere in una tabella le informazioni sui libri che possono matchare
 */
function print_book_matching_list($response)
{
    //I dati sono restituiti da google in formato json
    $json_response = json_decode($response);

    //Html che deve essere restituito
    $html = '<table>';

    foreach ($json_response->items as $item) {
        $html .= "<tr>";

        /**
         * Array che contiene gli attributi di ogni libro restituito dalla chiamata api.
         * Verranno utilizzati per precompilare i campi della creazione di un nuovo nodo
         * di tipo book grazie al modulo prepopulate
         * */
        $query = array();

        $volume_info = $item->volumeInfo;
        $html .= '<td>';
        if (isset($volume_info->imageLinks) && isset($volume_info->imageLinks->smallThumbnail)) {
            $html .= '<img src="' . $volume_info->imageLinks->smallThumbnail . '" />';
        }
        $html .= '</td><td>';

        $html .= isset($volume_info->title) ? $volume_info->title . '<br />' : '';
        $query["edit[title]"] = isset($volume_info->title) ? $volume_info->title : '';

        $html .= isset($volume_info->subtitle) ? $volume_info->subtitle . '<br />' : '';

        //Set authors
        if (isset($volume_info->authors)) {

            $query["edit[bidico_author][und]"] = "";

            foreach ($volume_info->authors as $author) {
                $html .= $author . " - ";
                //Elimina le virgole dal nome dell'autore perchè nel widget della tassonomia diventerebbero due autori diversi
                $query["edit[bidico_author][und]"] .= str_replace(","," ",$author) . ", ";
            }

            //Elimina l'ultima virgola aggiunta
            $query["edit[bidico_author][und]"] = rtrim($query["edit[bidico_author][und]"], ", ");
        }

        $html .= "<br />";

        //Set book description for query but don't show it
        if(isset($volume_info->description)) {
            $query["edit[body][und][0][value]"] = $volume_info->description;
        }

        //Cerca l'isbn tra i codici che vengono restituiti
        $isbn = '';
        $maybe_isbn = isset($volume_info->industryIdentifiers) ? $volume_info->industryIdentifiers : array();
        foreach($maybe_isbn as $id_code){
            if($id_code->type == "ISBN_13"){
                $isbn = $id_code->identifier;
                break;
            }
            if($id_code->type == "ISBN_10"){
                $isbn = $id_code->identifier;
            }
        }
        $html .= 'Cod. ' . $isbn . '<br />';
        $query["edit[bidico_isbn][und][0][value]"] = $isbn;

        $html .= t('<a href="@url">This is my book</a>',
            array('@url' => url(
                'node/add/book',
                array(
                    "query" => $query
                ))));
        $html .= "</tr>";
    }

    $html .= '</table>';

    //Aggiunge un link per inserire direttamente un uovo libro
    $html .= '<a href="' . url('node/add/book') .'">' . t('Add a book without correspondence') . '</a>';

    return $html;
}