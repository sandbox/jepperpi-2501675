<?php

/**
 *
 * Display table of transitions in which current user has role $role
 *
 * */
function bidico_show_transitions_table($role)
{
    //Current user
    global $user;

    $rows = array();
    $headers = array('book', 'state', 'approved', 'date');

    //Select transitions join user join node
    $query = db_select('transitions', 't');

    $query->join('node', 'book', 't.book_id = book.nid');

    $query->fields('book', array('title'));
    $query->fields('t', array('id', 'state', 'approved', 'date'));

    if ($role == 'owner') {
        $query->condition('owner_id', $user->uid, '=');
        $query->join('users', 'u', 't.borrower_id = u.uid');
        $headers[] = "borrower";
    } else {
        $query->condition('borrower_id', $user->uid, '=');
        $query->join('users', 'u', 't.owner_id = u.uid');
        $headers[] = "owner";
    }

    $query->fields('u', array('name'));

    $query->orderBy('date', 'DESC');

    $transitions = $query->execute();

    while ($record = $transitions->fetchAssoc()) {
        $row = $record;

        //L'id della trnsizione serve solo per creare il link per l'approvazione.
        // Viene eliminata dall'array perchè se no verrebbe visualizzato nella tabella
        $transition_id = $row['id'];
        unset($row['id']);

        //Aggiungi il link per approvare le prenotazioni
        if($row['state'] == 'reserved' && $row['approved'] == '0'){
            if($role == 'owner'){
                $row['approved'] = l(t("approve"),'bidico/transition/' . $transition_id . '/approve') . " - " .
                    l(t("deny"),'bidico/transition/' . $transition_id . '/deny');
            }else{
                $row['approved'] = t("pending");
            }
        }else if($row['state'] == 'reserved' && $row['approved'] == '2'){
            $row['approved'] = t("cancelled");
        }else if ($row['approved'] == '1'){
            $row['approved'] = t('approved');
        }

        //Trasforma timestamp in data
        $row['date'] = date('Y-m-d', $row['date']);

        //Get link to send message to user
        $row['name'] = l($row['name'],'messages/new',
            array('query' => array(
                'edit[recipient]' => $row['name'])));

        $rows[] = $row;
    }

    return (theme_table(
        array(
            'header' => $headers,
            'rows' => $rows,
            'attributes' => array(),
            'caption' => '',
            'colgroups' => array(),
            'sticky' => false,
            'empty' => t('No values'),
        )
    ));
}